strtools
---


Features
---

- Extract string from large text
- Validate string with large number of pattern
- Convert data


Usage
---
```bash
$ pip install strtools
```

Meta
----

Nguyen Khac Thanh - nguyenkhacthanh244@gmail.com

Distributed under the MIT license. See `LICENSE.txt <https://github.com/thanhngk/strtool/blob/master/LICENSE.txt>`_ for more information.

https://github.com/thanhngk/strtools
