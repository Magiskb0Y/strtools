import os
import sys

pkg = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(pkg)

from strtools import data


def test_data():
    d = [
        5,
        10,
        "20.577",
        "3",
        "Thanh",
        "4.0.5",
        {
            "message": "success",
            "code": 0
        },
        [2, 3, 5, {"name": "Thanh", "age": 20, "point": 17.5}]
    ]

    print("raw data", d)
    print(data.convert_true_type(d))

if __name__ == "__main__":
    test_json()
