import os
import sys

pkg = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(pkg)

from strtools import validators


def test_is_email():
    assert validators.is_email("nguyenkhacthanh244@gmail.com") == True
    assert validators.is_email("nguyenk@gmails.comsd") == False
    assert validators.is_email("nguyenkhac") == False
    assert validators.is_email("") == False
