"""Implement validators
"""


import re
from functools import wraps
from . import _patterns


def strip(func):
    """Strip all validators
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        try:
            value = kwargs.get("value")
            if not value:
                value = args[0]
            if value and value is str:
                value = value.strip()
        except Exception as err:
            raise ValueError(str(err))
        else:
            return func(*args, **kwargs)
    return decorator

@strip
def is_credit_card(value):
    """Check credit card
    Bank support: Visa, MasterCard, American Express,
                  Diners Club, Discover, JCB
    """
    check = re.fullmatch(_patterns.CREDIT_CARD, value)
    return check is not None

@strip
def is_c_style_comment(value):
    """Check C style comment
    // This is a line comment
    /*
     * This is a block comment
     */
    """
    check = re.fullmatch(_patterns.C_STYLE_COMMENT, value)
    return check is not None

@strip
def is_datetime(value):
    """Check datetime with format dd/mm/yy
    Example: 24/04/2015
    """
    check = re.fullmatch(_patterns.DATETIME, value)
    return check is not None

@strip
def is_email(value):
    """Check email
    """
    check = re.fullmatch(_patterns.EMAIL, value)
    return check is not None

@strip
def is_filepath_linux(value):
    """Check file path linux
    Example: /home/user/Documents/foo.log
    """
    check = re.fullmatch(_patterns.FILEPATH_LINUX, value)
    return check is not None

@strip
def is_filepath_win(value):
    """Check filepath Windows
    """
    check = re.fullmatch(_patterns.FILEPATH_WIN, value)
    return check is not None

@strip
def is_hex_color(value):
    """Check hex color value
    Example: fff or 00ff00 #fff or #00ff00
    """
    check = re.fullmatch(_patterns.HEX_COLOR_VALUE, value)
    return check is not None

@strip
def is_html_tag(value):
    """Check html value
    Example: <h1>HTML is a programming language</h1>
    """
    check = re.fullmatch(_patterns.HTML_TAG, value)
    return check is not None

@strip
def is_int(value):
    """Check integer value
    Example: 5.0 or 5
    """
    check = re.fullmatch(_patterns.INTEGER_NUMBER, value)
    return check is not None

@strip
def is_ipv4(value):
    """Check IPv4
    Example: 192.0.2.235
    """
    check = re.fullmatch(_patterns.IPV4, value)
    return check is not None

@strip
def is_ipv6(value):
    """Check ipv6
    Example: FE80::0202:B3FF:FE1E:8329
    """
    check = re.fullmatch(_patterns.IPV6, value)
    return check is not None

@strip
def is_negative_int(value):
    """Check negative int
    Example: -5
    """
    check = re.fullmatch(_patterns.NEGATIVE_INTEGER, value)
    return check is not None

@strip
def is_negative(value):
    """Check negative number
    Example: -5 or -5.3
    """
    check = re.fullmatch(_patterns.NEGATIVE_NUMBER, value)
    return check is not None

@strip
def is_phone_number(value):
    """
    Check phone number
    Example: 0123456789 or +84968512452
    """
    check = re.fullmatch(_patterns.PHONE_NUMBER, value)
    return check is not None

@strip
def is_positive_int(value):
    """Check pos integer
    """
    check = re.fullmatch(_patterns.POSITIVE_INTEGER, value)
    return check is not None

@strip
def is_positive(value):
    """Check pos numeber
    """
    check = re.fullmatch(_patterns.POSITIVE_NUMBER, value)
    return check is not None

@strip
def is_time24h(value):
    """Check time format 24h
    """
    check = re.fullmatch(_patterns.TIME_24H, value)
    return check is not None

@strip
def is_url(value):
    """Check url
    Example: http://google.com or https://localhost:5000/api/
    """
    check = re.fullmatch(_patterns.URL, value)
    return check is not None

@strip
def is_username(value):
    """Check simple username
    """
    check = re.fullmatch(_patterns.USERNAME, value)
    return check is not None

@strip
def is_youtube_channel_link(value):
    """Check youtube channel url
    Example: https://www.youtube.com/playlist?list=PL4Gm5cxlLzbt0thlO7uHcl2Pc4_t6Gqy3
    """
    check = re.fullmatch(_patterns.YOUTUBE_CHANNEL_LINK, value)
    return check is not None

@strip
def is_youtube_video_link(value):
    """Check video youtube
    Example: https://youtu.be/rvqhtoYY_cM
    """
    check = re.fullmatch(_patterns.YOUTUBE_VIDEO_LINK, value)
    return check is not None
