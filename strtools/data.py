from . import validators


def _convert_string(string):
    if validators.is_int(string):
        value = int(string)
    else:
        value = string
        try:
            value = float(string)
        except ValueError:
            pass
        finally:
            return value

def convert_true_type(iterable):
    if isinstance(iterable, list):
        new_list = []
        for item in iterable:
            if isinstance(item, str):
                item = _convert_string(item)
            else:
                item = convert_true_type(item)
            new_list.append(item)
        return new_list
    elif isinstance(iterable, dict):
        new_dict = {}
        for key, value in iterable.items():
            if isinstance(value, str):
                value = _convert_string(value)
            else:
                value = convert_true_type(value)
            new_dict.update({key: value})
        return new_dict
    return iterable
