"""Implement functions work with text
"""
import re


def regex_check(regex, value):
    """Shortcut function for check with regex
    """
    c = re.fullmatch(regex, value)
    return c is not None
