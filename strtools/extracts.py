"""Implement extract string module
"""


import re
from functools import wraps
from . import _patterns

def strip(func):
    """Strip all validators
    """
    @wraps(func)
    def decorator(*args, **kwargs):
        try:
            value = kwargs.get("value")
            if not value:
                value = args[0]
            if value and value is str:
                value = value.strip()
        except Exception as err:
            raise ValueError(str(err))
        else:
            return func(*args, **kwargs)
    return decorator

@strip
def extract_credit_card(value):
    """result credit card
    Bank support: Visa, MasterCard, American Express,
                  Diners Club, Discover, JCB
    """
    result = re.findall(_patterns.CREDIT_CARD, value)
    return result

@strip
def extract_c_style_comment(value):
    """result C style comment
    // This is a line comment
    /*
     * This is a block comment
     */
    """
    result = re.findall(_patterns.C_STYLE_COMMENT, value)
    return result

@strip
def extract_datetime(value):
    """result datetime with format dd/mm/yy
    Example: 24/04/2015
    """
    result = re.findall(_patterns.DATETIME, value)
    return result

@strip
def extract_email(value):
    """result email
    """
    result = re.findall(_patterns.EMAIL, value)
    return result

@strip
def extract_filepath_linux(value):
    """result file path linux
    Example: /home/user/Documents/foo.log
    """
    result = re.findall(_patterns.FILEPATH_LINUX, value)
    return result

@strip
def extract_filepath_win(value):
    """result filepath Windows
    """
    result = re.findall(_patterns.FILEPATH_WIN, value)
    return result

@strip
def extract_hex_color(value):
    """result hex color value
    Example: fff or 00ff00 #fff or #00ff00
    """
    result = re.findall(_patterns.HEX_COLOR_VALUE, value)
    return result

@strip
def extract_html_tag(value):
    """result html value
    Example: <h1>HTML is a programming language</h1>
    """
    result = re.findall(_patterns.HTML_TAG, value)
    return result

@strip
def extract_int(value):
    """result integer value
    Example: 5.0 or 5
    """
    result = re.findall(_patterns.INTEGER_NUMBER, value)
    return result

@strip
def extract_ipv4(value):
    """result IPv4
    Example: 192.0.2.235
    """
    result = re.findall(_patterns.IPV4, value)
    return result

@strip
def extract_ipv6(value):
    """result ipv6
    Example: FE80::0202:B3FF:FE1E:8329
    """
    result = re.findall(_patterns.IPV6, value)
    return result

@strip
def extract_negative_int(value):
    """result negative int
    Example: -5
    """
    result = re.findall(_patterns.NEGATIVE_INTEGER, value)
    return result

@strip
def extract_negative(value):
    """result negative number
    Example: -5 or -5.3
    """
    result = re.findall(_patterns.NEGATIVE_NUMBER, value)
    return result

@strip
def extract_phone_number(value):
    """
    result phone number
    Example: 0123456789 or +84968512452
    """
    result = re.findall(_patterns.PHONE_NUMBER, value)
    return result

@strip
def extract_positive_int(value):
    """result pos integer
    """
    result = re.findall(_patterns.POSITIVE_INTEGER, value)
    return result

@strip
def extract_positive(value):
    """result pos numeber
    """
    result = re.findall(_patterns.POSITIVE_NUMBER, value)
    return result

@strip
def extract_time24h(value):
    """result time format 24h
    """
    result = re.findall(_patterns.TIME_24H, value)
    return result

@strip
def extract_url(value):
    """result url
    Example: http://google.com or https://localhost:5000/api/
    """
    result = re.findall(_patterns.URL, value)
    return result

@strip
def extract_youtube_channel_link(value):
    """result youtube channel url
    Example: https://www.youtube.com/playlist?list=PL4Gm5cxlLzbt0thlO7uHcl2Pc4_t6Gqy3
    """
    result = re.findall(_patterns.YOUTUBE_CHANNEL_LINK, value)
    return result

@strip
def extract_youtube_video_link(value):
    """result video youtube
    Example: https://youtu.be/rvqhtoYY_cM
    """
    result = re.findall(_patterns.YOUTUBE_VIDEO_LINK, value)
    return result
