"""
Publish a new version:
$ git tag X.Y.Z -m "Release X.Y.Z"
$ git push --tags
$ pip install --upgrade twine wheel
$ python setup.py sdist bdist_wheel --universal
$ twine upload dist/*
"""
import codecs
import os
import sys
from setuptools import setup


STRTOOLS_VERSION = '0.1.1'
STRTOOLS_DOWNLOAD_URL = (
    'https://github.com/thanhngk/strtools/tarball/' + STRTOOLS_VERSION
)


def read_file(filename):
    """
    Read a utf8 encoded text file and return its contents.
    """
    with codecs.open(filename, 'r', 'utf8') as f:
        return f.read()


setup(
    name='strtools',
    packages=['strtools'],
    version=STRTOOLS_VERSION,
    description='Validate, format and extract information from string',
    long_description=read_file('README.md'),
    license='MIT',
    author='Nguyen Khac Thanh',
    author_email='nguyenkhacthanh244@gmail.com',
    url='https://github.com/thanhngk/strtools',
    download_url=STRTOOLS_DOWNLOAD_URL,
    keywords=[
        'strtools', 'str', 'validator', 'format', 'extract'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Natural Language :: English',
    ],
)
